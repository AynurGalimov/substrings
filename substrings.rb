def substrings(str, dictionary)
  str_array = str.downcase.split
  h = {}

  dictionary.each do |dict|
    count = 0
    str_array.each do |s|
      if s.include? dict
        count += 1
        h[dict] = count
      end
    end
  end

  h
end

dictionary = ['below','down','go','going','horn','how','howdy','it','i','low','own','part','partner','sit']
puts substrings('below', dictionary)
puts substrings('Howdy partner, sit down! How\'s it going?', dictionary)
