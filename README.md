# Substrings
The method returns a hash containing each substring that was found in the string, and the number of occurrences of this substring in the string.